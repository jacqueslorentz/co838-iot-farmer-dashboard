# CO838 - Assessment 3 - Data processing and visualisation

A dashboard to display data from IoT devices. Data come from a farmer's fields sensors.

## Getting Started
The project uses [Yarn](https://yarnpkg.com/en/) as packet manager.
To install dependencies:
```
yarn install
```
To run developement server:
```
yarn start
```
To create and serve production build:
```
yarn build && yarn serve
```

## Documentation

Displayed data are fetched from the ``http://shed.kent.ac.uk`` API. More over, there are some limits to process.
The dashboard should alert the farmer when data are outside folowing ranges.

For Greenhouse 1:
- 18-35 °C
- 300-700 TDS
- 50-75 %
- 10,000-50,000 lux

For Greenhouse 2:
- 18-27 °C
- 280-700 TDS
- 30-45 %
- 10,000-50,000 lux

For Greenhouse 3:
- 15-32 °C
- 40-55 %
- 10,000-50,000 lux

An alert is shown when data when a data is out of range, a warning is shown when data is near the limits.

## Marking Guideline

Data processing:
- `./src/API.js` for fetching, aggregation and fusion
- `./src/Conditions.js` for handling alerts and warning (no device, no sensor value or value out of range)

Visualisation:
All components, specially:
- `./src/SiteOverview.js` to display a site overview
- `./src/Graph.js` to use Chart.js library to display historic data as graphs
- `./src/MessageBox.js` in which are shown alert and warning message

Robustness:
- `./src/API.js` when found an error during fetching, insert ``null`` at value place
- `./src/Conditions.js` for handling alerts and warning (no device, no sensor value or value out of range)
- GraphQL third-party library usage, to secure data model and avoid unexpected object

## Built With

- [ReactJS](https://reactjs.org/) - Front end library used
- [Yarn](https://yarnpkg.com/en/) - Dependency Management
- [Redux](https://redux.js.org/) - Application state container
- [GraphQL](http://graphql.org/) - Query Language
- [Axios](https://github.com/axios/axios) - Used for HTTP requests
- [Redux Data Fetching](https://github.com/alanzanattadev/redux-data-fetching) - Redux GraphQL Management
- [Styled Component](https://www.styled-components.com/) - CSS JSX style
- [Recompose](https://github.com/acdlite/recompose) - React utiliy for high-order component
- [Semantic UI](https://react.semantic-ui.com) - User interface for web
- [Chart.js](https://www.chartjs.org/) - JS charting