import React from 'react';
import { Line } from 'react-chartjs-2';

const colors = ['#e77f67', '#63cdda', '#f5cd79', '#786fa6', '#596275'];

export default ({ history, label, devicesLabel }) => {
    const dataset = devicesLabel.map((name, index) => ({
        label: name,
        fill: false,
        data: history[index],
        borderColor: colors[index],
        backgroundColor: colors[index]
    }));
    return (
        <div>
            <Line
                data={{
                    labels: new Array(25).fill(0).map((v, i) => (i === 24 ? 'now' : `H-${24 - i}`)),
                    datasets: dataset
                }}
                options={{ animationSteps: 50, title: {
                    display: true,
                    text: label,
                    fontStyle: 'normal',
                    padding: 0,
                    fontSize: 14
                } }}
                height={100} width={200}
            />
        </div>
    );
}