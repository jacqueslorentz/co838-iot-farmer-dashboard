import axios from 'axios';

const BASE_URL = 'http://shed.kent.ac.uk';

// Get last 24h measures from a device sensor
const getMeasureHistory = (deviceID, measureID) => new Promise((resolve, reject) => {
    const to = new Date();
    let from = new Date()
    from.setTime(to.getTime() - 24*60*60*1000);
    axios.get(`${BASE_URL}/device/${deviceID}/${measureID}/hour?from=${from.toISOString()}&to=${to.toISOString()}`)
        .then((response) => resolve(response.data.map(hour => parseFloat(hour.mean).toFixed(2))))
        .catch((error) => resolve(null));
});

// Get a measure from a device
const getMeasure = (deviceID, measureID) => new Promise((resolve, reject) => {
    axios.get(`${BASE_URL}/device/${deviceID}/${measureID}`)
        .then((response) => {
            const value = parseFloat(response.data.value);
            resolve({
                time: response.data.time,
                value: (value > 100 && (measureID === 'battery' || measureID === 'moisture')) ? 100 : value
            });
        }).catch((error) => resolve(null));
});

// Get device information from site and device id
const getDevice = (siteID, id) => new Promise((resolve, reject) => {
    const deviceID = `${siteID}_${id}`
    axios.get(`${BASE_URL}/device/${deviceID}`, {
        transformResponse: [async (res) => {
            const device = JSON.parse(res);
            return {
                id: device.id,
                zone: device.zone,
                version: device.version,
                tds: await getMeasure(deviceID, 'tds'),
                tdsHistory: await getMeasureHistory(deviceID, 'tds'),
                light: await getMeasure(deviceID, 'light'),
                lightHistory: await getMeasureHistory(deviceID, 'light'),
                temperature: await getMeasure(deviceID, 'temperature'),
                temperatureHistory: await getMeasureHistory(deviceID, 'temperature'),
                moisture: await getMeasure(deviceID, 'moisture'),
                moistureHistory: await getMeasureHistory(deviceID, 'moisture'),
                battery: await getMeasure(deviceID, 'battery'),
                batteryHistory: await getMeasureHistory(deviceID, 'battery')
            }
        }]
    }).then(response => response.data.then(res => resolve(res)))
    .catch((error) => resolve(null));
});

// Fetch all sites
const getSites = (resolve, reject) => {
    axios.get(BASE_URL + '/sites', {
        transformResponse: [(res) => {
            return JSON.parse(res).map(async (site) => ({
                id: site.id,
                name: site.name,
                lat: site.lat,
                lng: site.lon,
                devices: await Promise.all(site.zones.map(zone => getDevice(site.id, zone.id)))
            }))
        }]
    }).then((response) => Promise.all(response.data).then(res => {
        console.log(res);
        resolve(res);
    }))
    .catch((error) => resolve(null));
};

export const request = () => new Promise(getSites);