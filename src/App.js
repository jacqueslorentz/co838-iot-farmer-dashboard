import React from 'react';
import { connect } from "react-redux";
import { compose, lifecycle } from "recompose";
import { DataFetcher } from "./configuration";
import styled from 'styled-components';
import { Dimmer, Loader } from 'semantic-ui-react';

import LeftSide from './LeftSide';
import RightSide from './RightSide';

const REFETCH_INTERVAL = 60 * 1000; // In milliseconds

const Container = styled.div`
    padding-top: 10px;
    display: flex;
    justify-content: space-around;
`;

const Loading = () => (
    <Dimmer active inverted>
        <Loader size='massive' inverted>Loading</Loader>
    </Dimmer>
);

function App ({ sites }) {
    if (!sites || sites.size === 0) {
        return <Loading />;
    }
    return (
        <div>
            <Container>
                <LeftSide sites={sites}/>
                <RightSide sites={sites}/>
            </Container>
        </div>
    );
}

export default compose(
  connect(
      ({ data }) => ({ data }),
      (dispatch) => ({ dispatch })
  ),
  DataFetcher({
      mapPropsToNeeds: (props) => `{
          sites {
              id, name, lat, lng, devices {
                  id, zone, version,
                  tds { time, value }, tdsHistory,
                  light { time, value }, lightHistory,
                  temperature { time, value }, temperatureHistory,
                  moisture { time, value }, moistureHistory,
                  battery { time, value }, batteryHistory,
              }
          }
      }`
  }),
  lifecycle({
      componentDidMount() {
          setInterval(() => {
              this.props.refetch();
          }, REFETCH_INTERVAL);
      }
  })
)(App);