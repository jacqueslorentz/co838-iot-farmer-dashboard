import React from 'react';
import styled from 'styled-components';
import { SimpleSelect } from 'react-selectize';
import { Icon, Button } from 'semantic-ui-react';
import { compose, withState } from 'recompose';

import 'react-selectize/themes/index.css'

import Graph from './Graph';

const Container = styled.div`
    width: 65%;
`;

const ColumnContainer = styled.div`
    display: flex;
    flex-direction: row;
    margin-top: 2px;
`;

const GraphContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flew-end;
    width: 50%;
`;

const MapContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
    align-items: center;
    padding-bottom: 15%;
    margin: auto;
`;

export default compose(
    withState('selected', 'setSelected', (props) => (props.sites.get(props.sites.size - 1).id)),
)(({ sites, selected, setSelected }) => {
    const site = sites.filter((s) => (s.id === selected)).get(0);
    return (
        <Container>
            <ColumnContainer>
                <GraphContainer>
                    <MapContainer style={{'height': (window.innerWidth * 0.1625) + 'px'}}>
                        <SimpleSelect
                            style={{'margin': 'auto'}}
                            placeholder="Select a site"
                            value={{ label: site.name, value: site.id }}
                            onValueChange={(value) => { if (value) { setSelected(value.value) } }}
                            options={sites.map((site) => ({ label: site.name, value: site.id }))._tail.array}
                        />
                        <Button onClick={() => {
                            window.open(`https://www.google.fr/maps/@${site.lat},${site.lng},356m/data=!3m1!1e3`, '_blank').focus();
                        }}><Icon name="map"/>Open position</Button>
                    </MapContainer>
                    <Graph
                        history={site.devices.map((d) => (d.lightHistory))}
                        devicesLabel={site.devices.map((d) => (d.id))}
                        label="Light measure"
                    />
                    <Graph
                        history={site.devices.map((d) => (d.temperatureHistory))}
                        devicesLabel={site.devices.map((d) => (d.id))}
                        label="Temperature measure"
                    />
                </GraphContainer>
                <GraphContainer>
                    <Graph
                        history={site.devices.map((d) => (d.moistureHistory))}
                        devicesLabel={site.devices.map((d) => (d.id))}
                        label="Moisture measure"
                    />
                    <Graph
                        history={site.devices.map((d) => (d.tdsHistory))}
                        devicesLabel={site.devices.map((d) => (d.id))}
                        label="TDS measure"
                    />
                    <Graph
                        history={site.devices.map((d) => (d.batteryHistory))}
                        devicesLabel={site.devices.map((d) => (d.id))}
                        label="Battery measure"
                    />
                </GraphContainer>
            </ColumnContainer>
        </Container>
    );
});