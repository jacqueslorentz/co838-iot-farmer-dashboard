const limits = {
    gh1: {
        tds: [300, 700],
        light: [10000, 50000],
        temperature: [18, 35],
        moisture: [50-75]
    },
    gh2: {
        tds: [280, 700],
        light: [10000, 50000],
        temperature: [18, 27],
        moisture: [30, 45]
    },
    gh3: {
        tds: null,
        light: [10000, 50000],
        temperature: [15, 32],
        moisture: [40, 55]
    },
    outside: {
        tds: null,
        light: null,
        temperature: null,
        moisture: null
    }
}

// Check if data value is outside or near limits
export const compareLimit = (site, measure, value) => {
    if (!limits[site][measure]) {
        return 0;
    }
    if (value > limits[site][measure][1] || value < limits[site][measure][0]) {
        return 2;
    }
    const range = (limits[site][measure][1] - limits[site][measure][0]) / 10;
    if (value > limits[site][measure][1] - range || value < limits[site][measure][0] + range) {
        return 1;
    }
    return 0;
}

// Check alert and warning for battery level
export const batteryAlert = (value) => (value > 50 ? 0 : (value > 20 ? 1 : 2));

// Get average value for a site, between each device (not use a non valid value from sensor)
export const siteAverage = (site) => {
    const values = site.devices.reduce((acc, elem) => {
        return {
            tdsTotal: acc.tdsTotal + (elem.tds ? 1 : 0),
            tds: acc.tds + (elem.tds ? elem.tds.value : 0),

            lightTotal: acc.lightTotal + (elem.light ? 1 : 0),
            light: acc.light + (elem.light ? elem.light.value : 0),

            temperatureTotal: acc.temperatureTotal + (elem.temperature ? 1 : 0),
            temperature: acc.temperature + (elem.temperature ? elem.temperature.value : 0),

            moistureTotal: acc.moistureTotal + (elem.moisture ? 1 : 0),
            moisture: acc.moisture + (elem.moisture ? elem.moisture.value : 0),

            batteryTotal: acc.batteryTotal + (elem.battery ? 1 : 0),
            battery: acc.battery + (elem.battery ? elem.battery.value : 0)
        }
    }, {
        tds: 0, tdsTotal: 0,
        light: 0, lightTotal: 0,
        temperature: 0, temperatureTotal: 0,
        moisture: 0, moistureTotal: 0,
        battery: 0, batteryTotal: 0
    });

    return {
        tds: values.tdsTotal > 0 ? (values.tds / values.tdsTotal).toFixed(0) : null,
        light: values.lightTotal > 0 ? (values.light / values.lightTotal).toFixed(0) : null,
        temperature: values.temperatureTotal > 0 ? (values.temperature / values.temperatureTotal).toFixed(1) : null,
        moisture: values.moistureTotal > 0 ? (values.moisture / values.moistureTotal).toFixed(1) : null,
        battery: values.batteryTotal > 0 ? (values.battery / values.batteryTotal).toFixed(values.battery / values.batteryTotal === 100 ? 0 : 1) : null,
    }
}

// Return site by id
const searchSite = (sites, id) => {
    return sites.filter((s) => (s.id === id)).get(0);
}

// Search if a elem with id is in array
const notDetectedError = (array, id, name) => {
    if (array.filter((s) => (s.id === id)).length === 0 || array.filter((s) => (s.id === id)).size === 0) {
        return { alert: true, content: `${id} ${name} is not detected` };
    }
    return null;
}

// Check device for a site
const searchDevice = (devices, site) => {
    return [
        notDetectedError(devices, site + '_east', 'device'),
        notDetectedError(devices, site + '_north', 'device'),
        notDetectedError(devices, site + '_south', 'device'),
        notDetectedError(devices, site + '_west', 'device')
    ];
}

// Get all alert and warning message
export const getAlertMessages = (sites) => {
    let messages = [];
    messages.push(notDetectedError(sites, 'gh1', 'field'));
    messages.push(notDetectedError(sites, 'gh2', 'field'));
    messages.push(notDetectedError(sites, 'gh3', 'field'));
    messages.push(notDetectedError(sites, 'outside', 'field'));
    messages.concat(searchDevice(searchSite(sites, 'gh1').devices, 'gh1'));
    messages.concat(searchDevice(searchSite(sites, 'gh2').devices, 'gh2'));
    messages.concat(searchDevice(searchSite(sites, 'gh3').devices, 'gh3'));
    messages.push(notDetectedError(searchSite(sites, 'outside').devices, 'outside_field', 'device'));

    sites.map((site) => {
        return site.devices.map((device) => {
            return ['tds', 'light', 'temperature', 'moisture', 'battery'].map((key) => {
                if (device[key] === null) {
                    messages.push({ alert: true, content: `Device ${device.id} cannot measure ${key}` });
                }
                if (device[key + 'History'] === null || device[key + 'History'].length < 24) {
                    messages.push({ alert: false, content: `Device ${device.id} cannot retrive all ${key} history measures` });
                }

                if (device[key]) {
                    const alert = (
                        key === 'battery' ?
                        batteryAlert(device.battery.value) :
                        compareLimit(site.id, key, device[key].value)
                    );
                    if (alert > 0) {
                        messages.push({ alert: (alert === 2), content:
                            key !== 'battery' ?
                            `Measure of ${key} for ${device.id} is ${alert === 2 ? 'not in range' : 'near the limit'}` :
                            `Batterie level of ${device.id} is below ${alert === 2 ? '20' : '50'} %`
                        });
                    }
                    const time = new Date(device[key].time).getTime();
                    const now = new Date().getTime();

                    if (now - 24 * 60*60*1000 > time) {
                        messages.push({ alert: true, content: `Outdated data (more than one day) from ${device.id} for ${key}` });
                    } else if (now - (2 * 60*60*1000) > time) {
                        messages.push({ alert: false, content: `Outdated data (less than one day) from ${device.id} for ${key}` });
                    }
                }
                return null;
            });
        });
    });

    return messages.filter((m) => (m !== null));
};