import { buildSchema } from "graphql";
import { configure } from 'redux-data-fetching';
import { compose, createStore, combineReducers, applyMiddleware } from 'redux';

import { request } from './API';

const graphQLSchema = buildSchema(`
    type Query {
        sites: [Site]
    }

    type Site {
        id: String,
        name: String,
        lat: Float,
        lng: Float,
        devices: [Device]
    }

    type Device {
        id: String,
        zone: String,
        version: String,
        tds: Measure,
        tdsHistory: [Float],
        light: Measure,
        lightHistory: [Float],
        temperature: Measure,
        temperatureHistory: [Float],
        moisture: Measure,
        moistureHistory: [Float],
        battery: Measure,
        batteryHistory: [Float]
    }

    type Measure {
        value: Float,
        time: String
    }
`);

const rootValue = {
    sites: () => request()
};

let { reducer, middleware, DataFetcher } = configure(graphQLSchema, {somecontext: "ok", api: {}}, rootValue);
const store = createStore(combineReducers({ data: reducer }), {}, compose(applyMiddleware(middleware)));

export { DataFetcher, store };
