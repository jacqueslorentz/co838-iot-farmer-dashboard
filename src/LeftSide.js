import React from 'react';
import styled from 'styled-components';
import MessageBox from './MessageBox';
import SiteOverview from './SiteOverview';
import { getAlertMessages } from './Conditions';

const Title = styled.h1`
    font-weight: normal;
    text-align: center;
`;

const Container = styled.div`
    display: flex;
    flex-direction: column;
    width: 30%;
`;

const Info = styled.p`
    font-size: 1.3em;
    margin: 0 0 5px 5px;
`;

export default ({ sites }) => {
    const data = getAlertMessages(sites);
    const importants = data.filter((e) => (e.alert === true));
    return (
        <Container>
            <Title>Farmer Cooksey's Dashboard</Title>
            <Info>
                {data.length} realtime notification{data.length > 1 ? 's' : ''}
                {importants.length ? ` (${importants.length} important${importants.length > 1 ? 's' : ''})` : ''}:
            </Info>
            <MessageBox data={data}/>
            {sites.map((site, index) => <SiteOverview key={index} isFirst={index === 0} site={site} />)}
        </Container>
    );
}