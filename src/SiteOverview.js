import React from 'react';
import styled from 'styled-components';
import { Icon } from 'semantic-ui-react';
import { siteAverage, compareLimit, batteryAlert } from './Conditions';

const FieldContainer = styled.div`
    display: flex;
    flex-direction: row;
    font-size: 1.5em;
    align-items: center;
`;

const FieldName = styled.p`
    font-size: 1.2em;
    padding-left: 10px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
`;

const Field = ({ large, icon, value, suffix, alert }) => {
    const color = (!alert ? 'inherit' : (alert === 1 ? 'orange' : 'red'));
    return (
        <FieldContainer style={{'width': large ? '60%' : '35%', 'color': (value ? color : 'red')}}>
            <Icon name={icon} />
            {value ? <FieldName>{value}{suffix}</FieldName> : <Icon name="exclamation triangle" />}
        </FieldContainer>
    );
}

const Line = styled.div`
    height: 25%;
    display: flex;
    flex-direction: row;
    justify-content: space-around;
`;

const Container = styled.div`
    margin: 4px;
    width: 100%;
    height: 140px;
    display: flex;
    flex-direction: column;
    justify-content: center;
`;

const getBatteryIconName = (level) => {
    const values = ['empty', 'quarter', 'half', 'three quarters', 'full'];
    const index = parseInt(level / 25, 10);
    return 'battery ' + values[index];
}

export default ({ isFirst, site }) => {
    const siteAvg = siteAverage(site);
    return (
        <Container style={{'borderTop': isFirst ? 'none' : '1px solid #ccc'}}>
            <Line>
                <Field icon="compass" value={site.name} large={true}></Field>
                <Field icon={getBatteryIconName(siteAvg.battery)} value={siteAvg.battery} suffix=" %" alert={batteryAlert(siteAvg.battery)}></Field>
            </Line>
            <Line>
                <Field icon="sun" value={siteAvg.light} suffix=" lux" large={true}  alert={compareLimit(site.id, 'light', siteAvg.light)}></Field>
                <Field icon="thermometer half" value={siteAvg.temperature} suffix=" °C"  alert={compareLimit(site.id, 'temperature', siteAvg.temperature)}></Field>
            </Line>
            <Line>
                <Field icon="leaf" value={siteAvg.tds} suffix=" TDS" large={true}  alert={compareLimit(site.id, 'tds', siteAvg.tds)}></Field>
                <Field icon="tint" value={siteAvg.moisture} suffix=" %"  alert={compareLimit(site.id, 'moisture', siteAvg.moisture)}></Field>
            </Line>
        </Container>
    );
}