import React from 'react';
import styled from 'styled-components';
import { Icon } from 'semantic-ui-react';

const Container = styled.div`
    border: 1px solid #ccc;
    border-radius: 10px;
    width: 100%;
    height: 150px;
    margin-bottom: 15px;
    overflow: scroll;
`;

const MessageContainer = styled.div`
    padding: 5%;
    height: 60px;
    font-size: 1.2em;
    display: flex;
    align-items: center;
`;

const Message = ({ message, isFirst }) => {
    return (
        <MessageContainer style={{
            'borderTop': isFirst ? 'none' : '1px solid #ccc',
            'color': (message.alert ? 'red' : 'orange')
        }}>
            <Icon name="exclamation triangle" style={{ 'marginRight': '15px', 'fontSize': '1.4em' }}/>
            <p>{message.content}</p>
        </MessageContainer>
    );
}

export default ({ data }) => {
    return (
        <Container>
            {data.sort((a, b) => (!a.alert)).map((elem, i) => (
                <Message key={i} isFirst={i === 0} message={elem} />
            ))}
        </Container>
    );
}